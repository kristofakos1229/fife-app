// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional

var name, email, photoUrl, uid, emailVerified;

//Get elements
var txtEmail, txtPassword, btnLogin, btnSignup, btnLogout, openModal, imgUrl, lblName

let searchValue = "";

var firebaseConfig = {
  apiKey: "AIzaSyDtxKGHmZsnpg2R7CKdkLl8oNSag9lHykI",
  authDomain: "fife-app.firebaseapp.com",
  databaseURL: "https://fife-app-default-rtdb.firebaseio.com",
  projectId: "fife-app",
  storageBucket: "fife-app.appspot.com",
  messagingSenderId: "235592798960",
  appId: "1:235592798960:web:39d151f49b45c29ef82835",
  measurementId: "G-10X8R8XT3L"
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);
var user = firebase.auth().currentUser;
var database = firebase.database();

const content = document.getElementById('content');

function loadContent(contentName) {
  return new Promise(resolve => {
    $("#content").load('content/'+contentName);
    resolve('resolved');
  });
  
}

load();

var getUrlParameter = function getUrlParameter(sParam) {
  var sPageURL = decodeURI(window.location.hash.split('?')[1]),
      sURLVariables = sPageURL.split('&'),
      sParameterName,
      i;

  for (i = 0; i < sURLVariables.length; i++) {
      sParameterName = sURLVariables[i].split('=');

      if (sParameterName[0] === sParam) {
          return typeof sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
      }
  }
  return false;
};

$(function() {

  var newHash      = "",
      $mainContent = $("#content"),
      $pageWrap    = $("body"),
      baseHeight   = 0,
      $el;
      
  $pageWrap.height($pageWrap.height());
  baseHeight = $pageWrap.height() - $mainContent.height();
  
  $("nav").delegate("a", "click", function() {
      attr = $(this).attr("href");
      window.location.hash = attr;
      //console.log("oldhash: "+attr+" hash: "+window.location.hash);
      return false;
  });
  
  $(window).bind('hashchange', async function(){

  
      newHash = window.location.hash.substring(1);

      //if(newHash.match(/([a-z]*)(.htm)\w/g)) {
      if (newHash) {
        await loadContent(newHash);
        $("li a").removeClass("current");
        $("li a[href='"+newHash+"']").addClass("current");
      };
    //} else window.location = newHash;
      
  });
  
  $(window).trigger('hashchange');

});

function load() {

  txtEmail = document.getElementById('txtEmail');
  txtPassword = document.getElementById('txtPassword');
  btnLogin = document.getElementById('btnLogin');
  btnSignup = document.getElementById('btnSignup');
  btnLogout = document.getElementById('btnLogout');
  openModal = document.getElementById('openModal');
  imgUrl = document.getElementById("imgUrl");
  lblName = document.getElementById("lblName");
  contentList = document.getElementById("contentList");
  searchBar = document.getElementById("search");
  submitBtn = document.getElementById("submit");

  $('#submit').on('click', e =>{
    window.location.hash = 'search.html?val='+searchBar.value;
  });

  openModal.addEventListener('click',e=>{
    $('#exampleModal').modal('show');
  })

  firebase.auth().onAuthStateChanged(firebaseUser => {
    if (firebaseUser) {
        $('#exampleModal').modal('hide')
        btnLogout.classList.remove("d-none");
        lblName.classList.remove("d-none");
        openModal.classList.add("d-none");
        if (firebaseUser != null) {
          displayName = firebaseUser.displayName;
          email = firebaseUser.email;
          lblName.innerHTML = email;
          //imgUrl.setAttribute("src",photoUrl = firebaseUser.photoURL);
          emailVerified = firebaseUser.emailVerified;
          uid = firebaseUser.uid;   // The user's ID, unique to the Firebase project. Do NOT use
                                    // this value to authenticate with your backend server, if
                                    // you have one. Use User.getToken() instead.

          sessionStorage.setItem('uid', uid);

                
          myPeopleRef = firebase.database().ref('users/'+uid);
          myPeopleRef.on('value', (snapshot) => {
            const data = snapshot.val();
            sessionStorage.setItem('name', data.name);
          });
          //if (displayName == null)
          //  location = "#register.html";
        }
        
    } else {
        console.log('not logged in');
        btnLogout.classList.add("d-none");
        openModal.classList.remove("d-none");
        lblName.classList.add("d-none");
        lblName.innerHTML = "";
        imgUrl.setAttribute("src","");
    }
  
  firebase.auth().setPersistence(firebase.auth.Auth.Persistence.SESSION)
    .then(() => {
      return firebase.auth().signInWithEmailAndPassword(email, password);
    })
    .catch((error) => {
      var errorCode = error.code;
      var errorMessage = error.message;
    });
  
  
  btnLogin.addEventListener('click', e => {
      const email = txtEmail.value;
      const pass = txtPassword.value;
      const auth = firebase.auth();
  
      const promise = auth.signInWithEmailAndPassword(email, pass);
  
      promise.catch(e => console.log(e.message));
  })
  
  btnSignup.addEventListener('click', e => {
      const email = txtEmail.value;
      const pass = txtPassword.value;
      const auth = firebase.auth();
  
      const promise = auth.createUserWithEmailAndPassword(email, pass);
  
      promise
          .catch(e => console.log(e.message));
  })
  
  btnLogout.addEventListener('click', e => {
    firebase.auth().signOut();
  })
});
}

function urlify(text) {
  var urlRegex = /(([a-z]+:\/\/)?(([a-z0-9\-]+\.)+([a-z]{2}|aero|arpa|biz|com|coop|edu|gov|info|int|jobs|mil|museum|name|nato|hu|net|org|pro|travel|local|internal))(:[0-9]{1,5})?(\/[a-z0-9_\-\.~]+)*(\/([a-z0-9_\-\.]*)(\?[a-z0-9+_\-\.%=&amp;]*)?)?(#[a-zA-Z0-9!$&'()*+.=-_~:@/?]*)?)(\s+|$)/gi;
  return text.replace(urlRegex, function(url) {
    return '<a class="outside" href="//' + url + '">' + url + '</a>';
  })
  // or alternatively
  // return text.replace(urlRegex, '<a href="$1">$1</a>')
}

function isVowel(char){
  if ("aáeéiíoóöőuúüú".indexOf(char) >= 0) return true;
  return false;
}

function toLong(vowel){
  short = "aeioöuü";
  long =  "áéíóőúű";
  i = short.indexOf(vowel);
  if (i >= 0)
    return long.slice(i,i+1)
  else return vowel;
}

function toldalek(str,toldalek){
  if (isVowel(str.slice(-1))) return str.slice(0,-1)+toLong(str.slice(-1))+toldalek;
  else if (str.slice(-1) == str.slice(-2,-1) && str.slice(-2,-1) == toldalek.slice(0,1)) return str+toldalek.slice(1);
  return "no";//else return str+toldalek;
}

function embedWord(str) {
  if (isVowel(str.substring(0,1))) {
    return "az "+str;
  }
  return "a "+str;
}
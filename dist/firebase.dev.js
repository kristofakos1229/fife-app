"use strict";

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
var name, email, photoUrl, uid, emailVerified; //Get elements

var txtEmail, txtPassword, btnLogin, btnSignup, btnLogout, openModal, imgUrl, lblName;
var firebaseConfig = {
  apiKey: "AIzaSyDtxKGHmZsnpg2R7CKdkLl8oNSag9lHykI",
  authDomain: "fife-app.firebaseapp.com",
  databaseURL: "https://fife-app-default-rtdb.firebaseio.com",
  projectId: "fife-app",
  storageBucket: "fife-app.appspot.com",
  messagingSenderId: "235592798960",
  appId: "1:235592798960:web:39d151f49b45c29ef82835",
  measurementId: "G-10X8R8XT3L"
}; // Initialize Firebase

firebase.initializeApp(firebaseConfig);
var appCheck = firebase.appCheck(); // Pass your reCAPTCHA v3 site key (public key) to activate(). Make sure this
// key is the counterpart to the secret key you set in the Firebase console.

appCheck.activate('6LcSls0bAAAAAKWFaKLih15y7dPDqp9qMqFU1rgG', // Optional argument. If true, the SDK automatically refreshes App Check
// tokens as needed.
true);
var user = firebase.auth().currentUser;
var database = firebase.database();
var provider = new firebase.auth.FacebookAuthProvider();
firebase.auth().languageCode = 'hu';
provider.setCustomParameters({
  'display': 'popup'
});
var content = document.getElementById('content');

function loadContent(contentName) {
  return new Promise(function (resolve) {
    console.log('loaded');
    $("#content").load('content/' + contentName + '.html');
    resolve('resolved');
  });
}

load();

var getUrlParameter = function getUrlParameter(sParam) {
  var sPageURL = decodeURI(window.location.hash.split('?')[1]),
      sURLVariables = sPageURL.split('&'),
      sParameterName,
      i;

  for (i = 0; i < sURLVariables.length; i++) {
    sParameterName = sURLVariables[i].split('=');

    if (sParameterName[0] === sParam) {
      return _typeof(sParameterName[1]) === undefined ? true : decodeURIComponent(sParameterName[1]);
    }
  }

  return sessionStorage.getItem('uid');
};

(function () {
  'use strict'; // Fetch all the forms we want to apply custom Bootstrap validation styles to

  var forms = document.querySelectorAll('.needs-validation'); // Loop over them and prevent submission

  Array.prototype.slice.call(forms).forEach(function (form) {
    form.addEventListener('submit', function (event) {
      if (!form.checkValidity()) {
        event.preventDefault();
        event.stopPropagation();
      } else saveAll();

      form.classList.add('was-validated');
    }, false);
  });
})();

$(function () {
  var newHash = "",
      $mainContent = $("#content"),
      $pageWrap = $("body"),
      baseHeight = 0,
      $el;
  $pageWrap.height($pageWrap.height());
  baseHeight = $pageWrap.height() - $mainContent.height();
  $("nav").delegate("a", "click", function () {
    attr = $(this).attr("href");
    window.location.hash = attr.split('.')[0];
    console.log("oldhash: " + attr + " hash: " + window.location.hash);
    return false;
  });
  $(window).bind('hashchange', function _callee() {
    return regeneratorRuntime.async(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            newHash = window.location.hash.substring(1); //if(newHash.match(/([a-z]*)(.htm)\w/g)) {

            if (!newHash) {
              _context.next = 6;
              break;
            }

            _context.next = 4;
            return regeneratorRuntime.awrap(loadContent(newHash.split('.')[0]));

          case 4:
            $("li a").removeClass("current");
            $("li a[href='" + newHash + "']").addClass("current");

          case 6:
            ; //} else window.location = newHash;

          case 7:
          case "end":
            return _context.stop();
        }
      }
    });
  });
  $(window).trigger('hashchange');
});

function login(firebaseUser) {
  if (firebaseUser) {
    $('#exampleModal').modal('hide');
    btnLogout.classList.remove("d-none");
    imgUrl.classList.remove("d-none");
    openModal.classList.add("d-none");

    if (firebaseUser != null) {
      displayName = firebaseUser.displayName;
      email = firebaseUser.email; //imgUrl.setAttribute("src",photoUrl = firebaseUser.photoURL);

      emailVerified = firebaseUser.emailVerified;
      uid = firebaseUser.uid; // The user's ID, unique to the Firebase project. Do NOT use
      // this value to authenticate with your backend server, if
      // you have one. Use User.getToken() instead.

      sessionStorage.setItem('uid', uid);
      var storage = firebase.storage();
      var pathReference = storage.ref('profiles/' + uid + '/profile.jpg');
      pathReference.getDownloadURL().then(function (url) {
        imgUrl.setAttribute('src', url);
        $('#imgUrl').on('load', function (e) {
          imgUrl.classList.remove('d-none');
          imgUrl.classList.remove('loading');
        });
      })["catch"](function (error) {
        console.log(error);
      });
      newMessages = firebase.database().ref('users/' + uid);
      newMessages.on('child_changed', function (data) {
        addMessage();
      });
      myPeopleRef = firebase.database().ref('users/' + uid);
      myPeopleRef.on('value', function (snapshot) {
        var data = snapshot.val();
        sessionStorage.setItem('name', data.name);
      }); //if (displayName == null)
      //  location = "#register.html";
    }
  } else {
    console.log('not logged in');
    btnLogout.classList.add("d-none");
    openModal.classList.remove("d-none");
    imgUrl.classList.add("d-none");
    imgUrl.setAttribute("src", "");
  }
}

function load() {
  //loadContent('home');
  window.location.hash = "home";
  txtEmail = document.getElementById('txtEmail');
  txtPassword = document.getElementById('txtPassword');
  btnLogin = document.getElementById('btnLogin');
  btnSwap = document.getElementById('btnSwap');
  btnSignup = document.getElementById('btnSignup');
  btnLogout = document.getElementById('btnLogout');
  openModal = document.getElementById('openModal');
  imgUrl = document.getElementById("imgUrl");
  lblName = document.getElementById("lblName");
  contentList = document.getElementById("contentList");
  searchBar = document.getElementById("search");
  submitBtn = document.getElementById("submit");
  $('#searchForm').on('submit', function (e) {
    e.preventDefault();
    window.location.hash = 'search.html?val=' + searchBar.value;
  });
  openModal.addEventListener('click', function (e) {
    $('#exampleModal').modal('show');
  });
  btnSwap.addEventListener('click', function (e) {
    $('#exampleModal').modal('hide');
    $('#exampleModal2').modal('show');
  });
  firebase.auth().onAuthStateChanged(function (firebaseUser) {
    login(firebaseUser);
  });
}

firebase.auth().setPersistence(firebase.auth.Auth.Persistence.SESSION).then(function () {
  return firebase.auth().signInWithEmailAndPassword(email, password);
})["catch"](function (error) {
  var errorCode = error.code;
  var errorMessage = error.message;
  console.log(errorCode);
  console.log(errorMessage);
});

function authWithCredential(credential) {
  // [START auth_facebook_signin_credential]
  // Sign in with the credential from the Facebook user.
  firebase.auth().signInWithCredential(credential).then(function (result) {
    // Signed in       
    var credential = result.credential; // ...
  })["catch"](function (error) {
    // Handle Errors here.
    var errorCode = error.code;
    var errorMessage = error.message; // The email of the user's account used.

    var email = error.email; // The firebase.auth.AuthCredential type that was used.

    var credential = error.credential; // ...
  }); // [END auth_facebook_signin_credential]
}

$('#signInForm').on('submit', function (e) {
  var email = txtEmail.value;
  var pass = txtPassword.value;
  var auth = firebase.auth();
  var promise = auth.signInWithEmailAndPassword(email, pass);
  promise["catch"](function (e) {
    return console.log(e.message);
  });
});
btnLogin.addEventListener('click', function (e) {});
btnSignup.addEventListener('click', function (e) {
  var email = $('#txtEmail2').val();
  var pass = $('#inputPassword').val();
  var passAgain = $('#inputPasswordAgain').val();

  if (pass == passAgain && checkPassword($('#txtPassword2').val()) > 0) {
    var auth = firebase.auth();
    var promise = auth.createUserWithEmailAndPassword(email, pass);
    promise["catch"](function (e) {
      return console.log(e.message);
    });
  } else console.log('nem egyeznek a jelszavak!');
});
btnLogout.addEventListener('click', function (e) {
  firebase.auth().signOut();
});

function checkPassword(password) {
  var sum = 0;
  if (password.match(/[a-z]+/)) sum += 25;
  if (password.match(/[A-Z]+/)) sum += 25;
  if (password.match(/[0-9]+/)) sum += 25;
  if (password.length >= 15) sum += 25;
  console.log(sum);
  return sum;
}

$('#fbLogin').on('click', function (e) {
  console.log('login'); // [START auth_facebook_signin_popup]

  firebase.auth().signInWithPopup(provider).then(function (result) {
    /** @type {firebase.auth.OAuthCredential} */
    var credential = result.credential; // The signed-in user info.

    var user = result.user;
    console.log(user);
    authWithCredential(credential); // This gives you a Facebook Access Token. You can use it to access the Facebook API.

    var accessToken = credential.accessToken; // ...
  })["catch"](function (error) {
    // Handle Errors here.
    var errorCode = error.code;
    var errorMessage = error.message; // The email of the user's account used.

    var email = error.email; // The firebase.auth.AuthCredential type that was used.

    var credential = error.credential;
    console.log(errorMessage);
    console.log(errorCode);
    console.log(email);
    console.log(credential); // ...
  }); // [END auth_facebook_signin_popup]
});

function urlify(text) {
  var urlRegex = /(([a-z]+:\/\/)?(([a-z0-9\-]+\.)+([a-z]{2}|aero|arpa|biz|com|coop|edu|gov|info|int|jobs|mil|museum|name|nato|hu|net|org|pro|travel|local|internal))(:[0-9]{1,5})?(\/[a-z0-9_\-\.~]+)*(\/([a-z0-9_\-\.]*)(\?[a-z0-9+_\-\.%=&amp;]*)?)?(#[a-zA-Z0-9!$&'()*+.=-_~:@/?]*)?)(\s+|$)/gi;
  return text.replace(urlRegex, function (url) {
    return '<a class="outside" href="//' + url + '">' + url + '</a>';
  }); // or alternatively
  // return text.replace(urlRegex, '<a href="$1">$1</a>')
}

function profToArray(prof) {
  prof = prof.split('$$');
  i = 0;
  end = [];
  prof.forEach(function (element) {
    newprof = element.split('$');

    if (newprof[0].trim() != '') {
      end.push([newprof[0], newprof[1]]);
      i++;
    }
  });
  return end;
}

String.prototype.replaceAt = function (index, replacement) {
  return this.substr(0, index) + replacement + this.substr(index + replacement.length);
};

String.prototype.lastVowel = function () {
  last = '';

  for (var _i = 0; _i < this.length; _i++) {
    if (isVowel(this[_i])) {
      last = this[_i];
    }
  }

  return last;
};

function isVowel(_char) {
  _char = _char.toLowerCase();
  if ("aáeéiíoóöőuúüú".indexOf(_char) >= 0) return true;
  return false;
}

function isLetter(str) {
  return str.length === 1 && str.match(/[a-z]/i);
}

function isCharHigh(_char2) {
  _char2 = _char2.toLowerCase();
  if ("eéiíöőüű".indexOf(_char2) >= 0) return true;
  return false;
}

function ToHigh(vowel) {
  vowel = vowel.toLowerCase();
  low = "aóo";
  high = "eőe";
  i = low.indexOf(vowel);
  if (i >= 0) return high.slice(i, i + 1);else return vowel;
}

function toLong(vowel) {
  vowel = vowel.toLowerCase();
  short = "aeoöuü";
  long = "áéóőúű";
  i = short.indexOf(vowel);
  if (i >= 0) return long.slice(i, i + 1);else return vowel;
}

function wordToHigh(word) {
  for (var _i2 = 0; _i2 < word.length; _i2++) {
    if (isVowel(word[_i2])) {
      word = word.replaceAt(_i2, ToHigh(word[_i2]));
    }
  }

  return word;
}

function getPitch(word) {
  high = 0;
  low = 0;

  for (var _i3 = 0; _i3 < word.length; _i3++) {
    if (isVowel(word[_i3])) {
      if (isCharHigh(word[_i3])) high++;else low++;
    }
  }

  if (low == 0 && high > 0) return "high";
  if (high == 0 && low > 0) return "low";
  if (low > 0 && high > 0) return "mixed";
  return "none";
}

function toldalek(str, toldalek) {
  if (str == '' || toldalek == '' || str.length < 3) return "...";
  str = str.trim();
  pitch = getPitch(str);

  if (pitch != "low" && !(pitch == 'mixed' && !isCharHigh(str.lastVowel()))) {
    toldalek = wordToHigh(toldalek);
  }

  if (isLetter(str.slice(-1))) {
    // betűvel végződik a szótő?
    // ha a toldalék magánhangzóval kezdődik
    if (isVowel(toldalek.slice(0, 1))) {
      //        szótő utolsó betűje     tő a vége nélkül  szótő utolsó betűje   toldalek első b nélkül
      if (isVowel(str.slice(-1))) {
        //if (str.slice(-1) == toldalek.slice(0,1)) return 
        return str.slice(0, -1) + toLong(str.slice(-1)) + toldalek.slice(1);
      }

      return str + toldalek;
    } else {
      if (isVowel(str.slice(-1))) return str.slice(0, -1) + toLong(str.slice(-1)) + toldalek;else if (str.slice(-1) == str.slice(-2, -1) && str.slice(-2, -1) == toldalek.slice(0, 1)) return str + toldalek.slice(1);
      return str + str.slice(-1) + toldalek.slice(1);
    }
  } else {
    return str + "-" + toldalek;
  }
}

function embedWord(str) {
  if (isVowel(str.substring(0, 1))) {
    return "az " + str;
  }

  if (str.length == 0) return "...";
  return "a " + str;
}

function addMessage() {}

$('#txtPassword2').on('input', function (e) {
  strength = checkPassword($('#txtPassword2').val());

  switch (strength) {
    case 0:
      col = "light";
      break;

    case 25:
      col = "light";
      break;

    case 50:
      col = "med";
      break;

    case 75:
      col = "strong";
      break;

    default:
      col = "strong";
      break;
  }

  $('#passwordStrength').attr("style", "width: " + strength + "%");
  $('#passwordStrength').attr("aria-valuenow", strength);
  $('#passwordStrength').removeClass("col-light");
  $('#passwordStrength').removeClass("col-med");
  $('#passwordStrength').removeClass("col-strong");
  $('#passwordStrength').addClass("col-" + col);
});

function getRandomGifs() {
  console.log('random');
  var gifs = document.getElementsByClassName("randomImg");

  for (var i = 0; i < gifs.length; i++) {
    randomGif(gifs[i]);
  }
}

function randomGif(element) {
  gifs = {
    cat: [0, 2, 13, 14, 15, 16, 18, 23],
    dog: [9, 16, 18, 22, 23, 26, 27, 31],
    city: [21, 18, 15, 33, 40, 26, 42, 117],
    //
    plant: [21, 18, 15, 33, 40, 26, 42, 117] //

  };
  type = sessionStorage.getItem('theme');
  var offset = gifs[type][Math.floor(Math.random() * gifs[type].length)];
  random = Math.floor(Math.random() * 200) + 1;
  console.log(random);
  var API = 'n7RVCSmaNfpwxsETUqNXHyZSuwb76yl9';
  fetch("https://api.giphy.com/v1/gifs/search?api_key=".concat(API, "&q=").concat(type, "&limit=1&offset=").concat(offset)).then(function (response) {
    return response.json();
  }).then(function (content) {
    console.log(content);
    $(element).css('background-image', 'url(' + content.data[0].images.downsized_large.url + ')');
  });
} //Photoshop,Web-fejlesztés,Modellezés,Gitár,Almám